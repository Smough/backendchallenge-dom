class BanksController < ApplicationController
    def index
        @banks = Bank.all
    end

    def show
        @bank = getBankById
    end

    def new
        @bank = Bank.new
    end

    def create
        @bank = Bank.new(parameters)
        if @bank.save
          redirect_to @bank
        else
          render 'new'
        end
    end

    def edit
        @bank = getBankById
    end

    def update
        @bank = getBankById
        if @bank.update(parameters)
            redirect_to @bank
        else
            render 'edit'
        end
    end

    def destroy
        @bank = getBankById
        @bank.destroy
        redirect_to banks_path
    end

    def transactionTotal
        @total = getBankById.transactions.sum(:amount)
    end
    
    private
    def getBankById
        Bank.find(params[:id])
    end
    def parameters
        params.require(:bank).permit(:name)
    end
end
