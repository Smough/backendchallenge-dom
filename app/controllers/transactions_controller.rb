class TransactionsController < ApplicationController
    def new
        @transaction = findBankById.transactions.build
    end
    
    def create
        @bank = findBankById
        @transaction =  @bank.transactions.create(parameters)
        if @transaction.save
            redirect_to bank_path(@bank)
        else
            render 'new'
        end
    end

    def destroy
        @transaction, @bank = findTransactionByIds
        @transaction.destroy

        redirect_to bank_path(@bank)
    end

    def edit
        @transaction, @bank = findTransactionByIds
    end

    def update
        @transaction, @bank = findTransactionByIds
        if (@transaction.update(parameters))
            redirect_to bank_path(@bank)
        else
            render 'edit'
        end
    end

    private
    def findBankById
        Bank.find(params[:bank_id])
    end

    def findTransactionByIds
        bank = findBankById
        return bank.transactions.find(params[:id]), bank
    end

    def parameters
        params.require(:transaction).permit(:amount, :date, :description)
    end
end
