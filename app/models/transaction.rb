class Transaction < ApplicationRecord
  belongs_to :bank
  validates :amount, presence: true
  validates :date, presence: true
end
