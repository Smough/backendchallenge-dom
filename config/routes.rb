Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'banks#index'

  resources :banks do
    resources :transactions
  end

  #route for sum of transaction for given bank
  get 'banks/:id/sum', to: 'banks#transactionTotal'
end
