class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.datetime :date
      t.text :description
      t.decimal :amount
      t.references :bank, null: false, foreign_key: true

      t.timestamps
    end
  end
end
