require 'test_helper'

class BankTest < ActiveSupport::TestCase
  test "GivenNewBankWithoutName WhenSaved ThenShouldFail" do
    bank = Bank.new
    assert_not bank.save
  end

  test "GivenNewValidBank WhenSaved ThenShouldSucceed" do
   bank = Bank.new(name: "test")
   assert bank.save
  end
end
