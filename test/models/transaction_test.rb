require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  
  test "GivenNewTransactionWithNegativeAmount WhenSaved ThenShouldSucceed" do
    @bank = banks(:one)
    transaction = Transaction.new(amount: (-20.85), date: DateTime.new(2020,2,3,4,5,6), description: "testDescription", bank_id: @bank.id)
    assert transaction.save   
  end

  test "GivenNewValidTransactionWithoutComment WhenSaved ThenShouldSucceed" do
    @bank = banks(:one)
    transaction = Transaction.new(amount: 200.85, date: DateTime.new(2020,2,3,4,5,6), bank_id: @bank.id)
    assert transaction.save   
  end

  test "GivenNewValidTransactionWithTooMuchDecimals WhenSaved ThenShouldSucceed" do
    @bank = banks(:one)
    transaction = Transaction.new(amount: 200.852, date: DateTime.new(2020,2,3,4,5,6), description: "testDescription", bank_id: @bank.id)
    assert transaction.save   
  end

  test "GivenNewTransactionWithoutDate WhenSaved ThenShouldFail" do
    @bank = banks(:one)
    transaction = Transaction.new(amount: 200.852, description: "testDescription", bank_id: @bank.id)
    assert_not transaction.save   
  end

  test "GivenNewTransactionWithoutAmount WhenSaved ThenShouldFail" do
    @bank = banks(:one)
    transaction = Transaction.new(:date => DateTime.new(2020,2,3,4,5,6), :description => "testDescription", bank_id: @bank.id)
    assert_not transaction.save   
  end

  test "GivenNewTransactionWithoutBank WhenSaved ThenShouldFail" do
    transaction = Transaction.new(:date => DateTime.new(2020,2,3,4,5,6), :description => "testDescription")
    assert_not transaction.save   
  end
end

